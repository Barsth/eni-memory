window.onload = init;

let valider1 =false;
let valider2 =false;
let valider3 =false;
let valider4 =false;
let increment;

function init() {
    document.getElementById("nomUtilisateur").addEventListener("input",longueurUserName);
    document.getElementById("email").addEventListener("input",verifMail);
    document.getElementById("verifMdp").addEventListener("input",verifVerifMdp);
    document.getElementById("mdp").addEventListener("input",verifVerifMdp);
    document.getElementById("mdp").addEventListener("input",verifMdp);
    document.getElementById("mdp").addEventListener("input",barreMdp);
    document.addEventListener("input",boutonValider);
    document.getElementById("valider").addEventListener("click",enregistrerUtilisateur);
}


function longueurUserName() {
    let saisie =document.getElementById("nomUtilisateur").value;
    let erreur=document.getElementById("erreurPseudo");
    let image=document.getElementById("userError");

    if (saisie.length>=3){
        erreur.style.visibility="hidden";
        image.src="images/ressources/check.svg";
        image.style.visibility="visible";
        valider1 =true;
    }else {
        erreur.style.visibility="visible";
        image.src="images/ressources/error.svg";
        image.style.visibility="visible";
        valider1 =false;
    }
    if ( saisie.length==0) {
        erreur.style.visibility="hidden";
        image.style.visibility="hidden";
        valider1 =false;
    }
}

function verifMail() {
    let saisie =document.getElementById("email").value;
    let erreur=document.getElementById("erreurMail");
    let image=document.getElementById("emailError");

    if (saisie.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g)){
        erreur.style.visibility="hidden";
        image.src="images/ressources/check.svg";
        image.style.visibility="visible";
        valider2 =true;
    }else {
        erreur.style.visibility="visible";
        image.src="images/ressources/error.svg";
        image.style.visibility="visible";
        valider2 =false;
    }
    if ( saisie.length==0) {
        erreur.style.visibility="hidden";
        image.style.visibility="hidden";
        valider2 =false;
    }
}

function verifVerifMdp() {
    let saisieMdp =document.getElementById("mdp").value;
    let saisieVerif =document.getElementById("verifMdp").value;
    let image=document.getElementById("verifError");

    if (saisieMdp==saisieVerif && saisieMdp.length>=6){
        image.src="images/ressources/check.svg";
        image.style.visibility="visible";
        valider3 =true;
    }else {
        image.src="images/ressources/error.svg";
        image.style.visibility="visible";
        valider3 =false;
    }
    if ( saisieVerif.length==0) {
        image.style.visibility="hidden";
        valider3 =false;
    }
}

function verifMdp() {
    let saisie =document.getElementById("mdp").value;
    let image=document.getElementById("mdpError")

    if (saisie.match(/[a-zA-Z]/g)
    && saisie.match(/[0-9]/g)
    && saisie.match(/[^0-9a-zA-Z]/g)
    && saisie.length>=6 ){
        image.src="images/ressources/check.svg"
        image.style.visibility="visible"
        valider4 =true;
    }else {
        image.src="images/ressources/error.svg"
        image.style.visibility="visible"
        valider4 =false;
    }if ( saisie.length==0) {
        image.style.visibility="hidden";
        valider4 =false;
    }
}

function barreMdp() {
    let barreFaible = document.getElementById("faible");
    let texteFaible = document.getElementById("pfaible");
    let barreMoyen = document.getElementById("moyen");
    let texteMoyen = document.getElementById("pmoyen");
    let barreFort = document.getElementById("fort");
    let texteFort = document.getElementById("pfort");
    let saisie = document.getElementById("mdp").value;

    if (saisie.length>=1) {
        barreFaible.style.visibility="visible";
        texteFaible.style.visibility="visible";
        barreMoyen.style.visibility="hidden";
        texteMoyen.style.visibility="hidden";
        barreFort.style.visibility="hidden";
        texteFort.style.visibility="hidden";
    }
    if (saisie.length>5 && (saisie.match(/[^0-9a-zA-Z]/g)||saisie.match(/[0-9]/g)) ) {
        barreFaible.style.visibility="visible";
        texteFaible.style.visibility="visible";
        barreMoyen.style.visibility="visible";
        texteMoyen.style.visibility="visible";
        barreFort.style.visibility="hidden";
        texteFort.style.visibility="hidden";
    }
    if (saisie.length>8 && saisie.match(/[^0-9a-zA-Z]/g) && saisie.match(/[0-9]/g) ) {
        barreFaible.style.visibility="visible";
        texteFaible.style.visibility="visible";
        barreMoyen.style.visibility="visible";
        texteMoyen.style.visibility="visible";
        barreFort.style.visibility="visible";
        texteFort.style.visibility="visible";
    }
    if ( saisie.length==0) {
        barreFaible.style.visibility="hidden";
        texteFaible.style.visibility="hidden";
        barreMoyen.style.visibility="hidden";
        texteMoyen.style.visibility="hidden";
        barreFort.style.visibility="hidden";
        texteFort.style.visibility="hidden";
    }

}

function boutonValider() {
    if (valider1==true && valider2==true && valider3==true && valider4==true) {
        document.getElementById("valider").disabled=false;
    } else {
        document.getElementById("valider").disabled=true;
    }
}

function enregistrerUtilisateur() {
    if (localStorage.getItem("increment")==null) {
        localStorage.setItem("increment",1)
    }else {
        let oui = parseInt(localStorage.getItem("increment"))+1;
        localStorage.setItem("increment",oui)
    }
    let incr=parseInt(localStorage.getItem("increment"))
    let utilisateur = document.getElementById("nomUtilisateur").value;
    let email = document.getElementById("email").value;
    let mdp = document.getElementById("mdp").value;
    let stop=0;
    for (let i = 1; i < incr+1; i++) {
        if (utilisateur==localStorage.getItem("utilisateur"+i)) {
            stop=1;
            break;
        }
        if (email==localStorage.getItem("email"+i)) {
            stop=1;
            break;
        }
    }
if (stop==0) {
    localStorage.setItem("utilisateur"+incr,utilisateur)
    localStorage.setItem("email"+incr,email)
    localStorage.setItem("mdp"+incr,mdp)
    window.location.href="connecter.html"
}else{
    let non = parseInt(localStorage.getItem("increment"))-1;
    localStorage.setItem("increment",non)
}
}