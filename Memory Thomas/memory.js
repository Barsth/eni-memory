window.onload = init;
let tampon =42;             //Tampon pour savoir si c'est la premiere ou la seconde carte
let cartePrecedente=777;    //Recoit l'image de la carte précedente pour comparer
let nbCoup = 0              //Compte les coups
let victoire = 0            //Compte les paires retournée

const tab = [
    "images/ressources/plantes/1.png",
    "images/ressources/plantes/1.png",
    "images/ressources/plantes/2.png",
    "images/ressources/plantes/2.png",
    "images/ressources/plantes/3.png",
    "images/ressources/plantes/3.png",
    "images/ressources/plantes/4.png",
    "images/ressources/plantes/4.png",
    "images/ressources/plantes/5.png",
    "images/ressources/plantes/5.png",
    "images/ressources/plantes/6.png",
    "images/ressources/plantes/6.png",
];

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }
  console.log(tab)
shuffle(tab); //Mélange des cases du tableau.
tab.push("images/ressources/question.PNG")

function init() {
    document.getElementById("1").addEventListener("click",retourner);
    document.getElementById("2").addEventListener("click",retourner);
    document.getElementById("3").addEventListener("click",retourner);
    document.getElementById("4").addEventListener("click",retourner);
    document.getElementById("5").addEventListener("click",retourner);
    document.getElementById("6").addEventListener("click",retourner);
    document.getElementById("7").addEventListener("click",retourner);
    document.getElementById("8").addEventListener("click",retourner);
    document.getElementById("9").addEventListener("click",retourner);
    document.getElementById("10").addEventListener("click",retourner);
    document.getElementById("11").addEventListener("click",retourner);
    document.getElementById("12").addEventListener("click",retourner);
    document.getElementById("tablal").addEventListener("click",decomptVictoire);
    document.addEventListener("keydown",relancerPartie)
}

function retourner(event) {
    let change = event.currentTarget;
    if (change.src.match(tab[12])) {      //Cette boucle empeche de cliquer sur une image non retournée
        if (tampon==42) {                //Cette boucle permet de différencier le premier click du second
            change.style.transform="rotateY(90deg)";
            setTimeout( () => {change.src=tab[change.id-1]}, 200);
            setTimeout( () => {change.style.transform="rotateY(0deg)"}, 200);
            tampon=tab[change.id-1]
            cartePrecedente=change
        } else if (cartePrecedente.src.match(tab[change.id-1])){    //compare si les deux images retournées sont identiques
            change.style.transform="rotateY(90deg)";
            setTimeout( () => {change.src=tab[change.id-1]}, 200);
            setTimeout( () => {change.style.transform="rotateY(0deg)"}, 200);
            tampon=42
            setTimeout( () => {cartePrecedente=222}, 200);
            victoire++
        } else {    //si les images sont différentes, retourne dans x secondes et empeche les clicks
            change.style.transform="rotateY(90deg)";
            setTimeout( () => {change.src=tab[change.id-1]}, 200);
            setTimeout( () => {change.style.transform="rotateY(0deg)"}, 200);
            document.getElementById("tablal").style.pointerEvents="none"
            setTimeout(() => {
                change.style.transform="rotateY(90deg)";
                setTimeout( () => {change.src=tab[12]}, 200);
                setTimeout( () => {change.style.transform="rotateY(0deg)"}, 200);
                cartePrecedente.style.transform="rotateY(90deg)";
                setTimeout( () => {cartePrecedente.src=tab[12]}, 200);
                setTimeout( () => {cartePrecedente.style.transform="rotateY(0deg)"}, 200);
                setTimeout( () => {cartePrecedente=222}, 200);
                tampon=42;
                setTimeout( () => {document.getElementById("tablal").style.pointerEvents="all"}, 200);
            }, 1000);
        }   
        nbCoup++
    }
}

function decomptVictoire() {     //Indique le nombre de coups, et si la partie est gagnée.
    let oui = document.getElementById("infoPartie")
    if (victoire<(tab.length-1)/2) {
        oui.innerText=("Vous avec fait "+Math.trunc(nbCoup/2)+" coups.")
    } else {
        oui.innerText=("VICTOIRE POUR LES PLANTES !! ( en "+Math.trunc(nbCoup/2)+" coups)")
        oui.style.transition= "transform 1s ease-in-out";
        oui.style.transform="scale(3,3)"
        document.getElementById("BOOM").style.visibility="visible"
    }
}

function relancerPartie(event){   //Relancer avec barre d'espace
    if (event.keyCode == "32"){
        location.reload()
    }
}